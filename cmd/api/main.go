package main

import (
	"context"
	"fmt"
	"log"
	"net/http"
	"os"
	"os/signal"
	"syscall"
	"time"

	"github.com/go-chi/chi/v5"

	"github.com/spf13/viper"
)

func initViper() {
	viper.SetConfigName("bpconfig")
	viper.SetConfigType("yaml")
	viper.AddConfigPath(".")

	err := viper.ReadInConfig()
	if err != nil {
		panic(fmt.Errorf("fatal error config file: %w", err.Error()))
	}

	viper.AutomaticEnv()
}

func GracefulShutdown(
	handler http.Handler,
	addr string,
	chanError chan error,
) {

	server := &http.Server{
		Addr:    fmt.Sprintf(":%s", addr),
		Handler: handler,
	}

	ctx, stop := signal.NotifyContext(
		context.Background(),
		os.Interrupt,
		syscall.SIGTERM,
		syscall.SIGQUIT,
	)

	go func() {
		<-ctx.Done()
		log.Println("Received shutdown signal. Shutting down server...")

		shutdownTimeout := 7 * time.Second
		ctxTimeout, cancel := context.WithTimeout(context.Background(), shutdownTimeout)

		defer func() {
			stop()
			cancel()
			close(chanError)
		}()

		err := server.Shutdown(ctxTimeout)
		if err != nil {
			return
		}
		log.Println("Server gracefully stopped.")
	}()

	log.Printf("Server up and running at http://localhost:%s", addr)

}

func main() {
	initViper()

	r := chi.NewRouter()

	r.Get("/", func(w http.ResponseWriter, r *http.Request) {
		w.Write([]byte("B+ API it is working"))
	})

	chanError := make(chan error)

	go GracefulShutdown(r, "3030", chanError)

	// log.Println("Server up and running...")

	if err := <-chanError; err != nil {
		log.Fatal(err)
	}
}
