define HEADER

 ____             _    ____ ___
| __ )   _       / \  |  _ \_ _|
|  _ \ _| |_    / _ \ | |_) | |
| |_) |_   _|  / ___ \|  __/| |
|____/  |_|   /_/   \_\_|  |___|

endef
export HEADER

VERSION ?= $(shell git describe --tags 2>/dev/null | cut -c 2-)

.PHONY: up
up:
	@echo "$$HEADER"
	docker compose up -d --build

.PHONY: restart
restart:
	@echo "$$HEADER"
	docker compose restart

.PHONY: logdb
logdb:
	@echo "$$HEADER"
	docker logs -f bplusdb

.PHONY: stop
stop:
	@echo "$$HEADER"
	docker compose stop

.PHONY: down
down:
	@echo "$$HEADER"
	docker compose down --remove-orphans

.PHONY: clean
clean:
	@echo "$$HEADER"
	docker system prune --all --force

.PHONY: dang
dang:
	@echo "$$HEADER"
	docker rmi -f $$(docker images -q -f dangling=true)

.PHONY: dangvol
dangvol:
	@echo "$$HEADER"
	docker volume rm -f $$(docker volume ls -q --filter dangling=true)

.PHONY: remove
remove:
	@echo "$$HEADER"
	docker rmi -f $$(docker ps -a -q)

.PHONY: psql
psql:
	@echo "$$HEADER"
	docker exec -it service-core-db psql -U postgres service-core

run-service-core-tests:
	@echo "$$HEADER"
	cd service-core && go test ./...

generate-service-core-protoc:
	@echo "$$HEADER"
	protoc --go_out=./service-core --go-grpc_out=./service-core ./service-core/protos/*.proto

# example: make release V=0.0.0
.PHONY: release
release:
	@echo "$$HEADER"
	git tag v$(V)
	@read -p "Press enter to confirm and push to origin ..." && git push origin v$(V)

gorun:
	go run -exec "/usr/bin/env GODEBUG=schedtrace=250,scheddetail=1" ./cmd/api/main.go
