FROM golang:alpine as builder

WORKDIR /app/cmd/api

 # Download Go modules
COPY go.mod go.sum ./
RUN go mod download

# Copy the source code.
COPY . ./

#Build
RUN CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -a -trimpath -installsuffix cgo -v -o bplusapi ./cmd/api/main.go

FROM scratch

# RUN adduser -S -D -H -h /app appuser
# USER appuser

LABEL maintainer="Waldir Borba Junior <wborbajr@gmail.com>" \
  version="v0.1.0-2023" \
  description="B+ API | waldirborbajr/bplusapi:latest"

WORKDIR /

COPY --from=builder /app/cmd/api ./

EXPOSE 3000

ENTRYPOINT ["./bplusapi"]
